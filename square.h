#ifndef SQUARE_H
#define SQUARE_H
#include <QtGlobal>
#include <string>

class Square
{
public:
    virtual qint32 atTheEntry(qint32 wallet) = 0;
    virtual qint32 atTheExit(qint32 wallet) = 0;
    virtual qint32 passOver(qint32 wallet) = 0;
    Square();
    virtual ~Square() = default;
    std::string getName() const;

protected:
    std::string name;
};

#endif // SQUARE_H
