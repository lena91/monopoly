#include "prizesquare.h"

qint32 PrizeSquare::atTheEntry(qint32 wallet)
{
    return wallet + 200;
}

qint32 PrizeSquare::atTheExit(qint32 wallet)
{
    return wallet;
}

qint32 PrizeSquare::passOver(qint32 wallet)
{
    return wallet;
}

PrizeSquare::PrizeSquare()
{
    name = "Prize Square";
}
