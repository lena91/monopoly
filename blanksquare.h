#ifndef BLANKSQUARE_H
#define BLANKSQUARE_H
#include "square.h"

class BlankSquare : public Square
{
public:
    qint32 atTheEntry(qint32 wallet);
    qint32 atTheExit(qint32 wallet);
    qint32 passOver(qint32 wallet);
    BlankSquare();
};

#endif // BLANKSQUARE_H
