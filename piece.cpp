#include "piece.h"

Piece::Piece(Color color) : color(color)
{

}

Piece::Color Piece::getColor() const
{
    return color;
}

quint8 Piece::getSquareID() const
{
    return squareID;
}

void Piece::setSquareID(const quint8 &value)
{
    squareID = value;
}
