#include "monopolygamecreator.h"
#include "monopolygame.h"
#include "textformat.h"
#include <cstdlib>

MonopolyGameCreator::MonopolyGameCreator()
{

}

void MonopolyGameCreator::CreateGame()
{
    MonopolyGame mono(getPlayersNamesFromInput(), getDiceSidesFromInput(), getBoardSizeFromInput());
    mono.PlayGame();
}

quint32 MonopolyGameCreator::getBoardSizeFromInput()
{
    quint32 boardSize;
    std::cout << "Podaj rozmiar planszy: ";
    std::cin >> boardSize;

    return boardSize;
}

std::vector<quint8> MonopolyGameCreator::getDiceSidesFromInput()
{
    int noOfDice{};
    quint16 dieSides{};
    std::vector<quint8> diceSides{};

    std::cout << "Podaj liczbe kosci do gry: ";
    std::cin >> noOfDice;

    for (auto i = 0; i < noOfDice; ++i)
    {
        std::cout << "Podaj liczbe scian " << i+1 << " kosci: ";
        std::cin >> dieSides;
        diceSides.push_back(dieSides);
    }

    return diceSides;
}

std::vector<std::string> MonopolyGameCreator::getPlayersNamesFromInput()
{
    int noOfPlayers{};
    std::string playerName{};
    std::vector<std::string> playersNames{};
    quint8 colorID = 31;

    std::cout << "Podaj liczbe graczy: ";
    std::cin >> noOfPlayers;

    for (auto i = 0; i < noOfPlayers; ++i)
    {
        std::string textToPrint = "Podaj imie " + std::to_string(i+1) + " gracza: ";
        std::cout << TextFormat::colorText(colorID++, textToPrint);
        //std::cout << "\033[1;" << static_cast<unsigned>(i + 31) << "m" << "Podaj imie " << i+1 << " gracza: " << "\033[0m";
        std::cin >> playerName;
        playersNames.push_back(playerName);
    }

    return playersNames;
}
