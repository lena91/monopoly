#include "die.h"

Die::Die(quint8 noOfFaces)
    : noOfFaces(noOfFaces)
{
    srand(time(NULL));
}

quint8 Die::randomRoll() const
{
    return rand() % noOfFaces + 1;
}
