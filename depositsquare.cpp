#include "depositsquare.h"

qint32 DepositSquare::atTheEntry(qint32 wallet)
{
    wallet += deposit;
    deposit = 0;
    return wallet;
}

qint32 DepositSquare::atTheExit(qint32 wallet)
{
    return wallet;
}

qint32 DepositSquare::passOver(qint32 wallet)
{
    wallet -= 50;
    deposit += 50;
    return wallet;
}

DepositSquare::DepositSquare()
{
    name = "Deposit Square";
}
