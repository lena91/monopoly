#ifndef TEXTFORMAT_H
#define TEXTFORMAT_H
#include <string>
#include <QtGlobal>

class TextFormat
{
public:
    TextFormat();
    static std::string colorText(quint8 colorID, std::string text);
};

#endif // TEXTFORMAT_H
