#ifndef MONOPOLYGAMECREATOR_H
#define MONOPOLYGAMECREATOR_H
#include <vector>
#include <string>
#include <iostream>
#include <QtGlobal>

class MonopolyGameCreator
{
public:
    MonopolyGameCreator();
    void CreateGame();
private:
    quint32 getBoardSizeFromInput();
    std::vector<quint8> getDiceSidesFromInput();
    std::vector<std::string> getPlayersNamesFromInput();

};

#endif // MONOPOLYGAMECREATOR_H
