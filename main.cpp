#include <QCoreApplication>
#include "monopolygamecreator.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    MonopolyGameCreator mgc;
    mgc.CreateGame();
    std::cin.get();

    return a.exec();
}
