#include "monopolygame.h"
#include "textformat.h"

MonopolyGame::MonopolyGame(std::vector<std::string> playersNames, std::vector<quint8> diceSides, quint32 boardSize)
{
    board = new Board(boardSize);

    for (quint16 dieSides : diceSides)
    {
        diceVector.push_back(Die(dieSides));
    }

    for (auto i = 0u; i < playersNames.size(); ++i)
    {
        Piece piece = Piece(Piece::Color(31 + i));
        Player player(playersNames[i], piece, 1500);
        playersVector.push_back(player);
    }
}

void MonopolyGame::PlayGame()
{
    bool endGame = false;
    quint8 diceRollSum;
    quint8 squareID;
    quint8 colorID;
    quint32 boardSize = board->getNoOfSquares();

    while (!endGame)
    {
        for (Player &player : playersVector)
        {
            colorID = static_cast<unsigned>(player.getPiece().getColor());

            std::string textToPrint = "Tura gracza " + player.getName();
            std::cout << TextFormat::colorText(colorID, textToPrint) << std::endl;

            //std::cout << "\033[1;" << static_cast<unsigned>(colorID) << "m" << "Tura gracza " << player.getName() << "\033[0m" << std::endl;

            diceRollSum = 0;

            for (Die const &die : diceVector)
            {
                diceRollSum += static_cast<unsigned>(die.randomRoll());
            }

            std::cout << "Pieniadze w portfelu na poczatku tury : " << player.getWallet() << std::endl;
            std::cout << "Suma wyrzuconych wynikow: " << static_cast<unsigned>(diceRollSum) << std::endl;

            Square *square;

            Piece piece = player.getPiece();
            squareID = player.getPiece().getSquareID();

            square = board->getSquaresVector()[static_cast<std::size_t>(squareID)];
            std::cout << "Pole nr " << static_cast<std::size_t>(squareID + 1) << " (" << square->getName() << ")" << std::endl;
            qint32 atExit = square->atTheExit(player.getWallet());
            std::cout << "Pieniadze na wyjsciu " << atExit << std::endl;
            player.setWallet(atExit);

            squareID = (squareID + 1) % boardSize;

            qint32 passOver;
            for (auto i = 0; i < diceRollSum - 1; ++i)
            {
                square = board->getSquaresVector()[static_cast<std::size_t>(squareID)];
                std::cout << "Pole nr " << static_cast<std::size_t>(squareID + 1) << " (" << square->getName() << ")" << std::endl;
                passOver = square->passOver(player.getWallet());
                std::cout << "Pieniadze przy przejsciu " << passOver << std::endl;
                player.setWallet(passOver);

                squareID = (squareID + 1) % boardSize;
            }
            square = board->getSquaresVector()[static_cast<std::size_t>(squareID)];
            std::cout << "Pole nr " << static_cast<std::size_t>(squareID + 1) << " (" << square->getName() << ")" << std::endl;
            qint32 atEntry = square->atTheEntry(player.getWallet());
            std::cout << "Pieniadze na wejsciu " << atEntry << std::endl;
            player.setWallet(atEntry);

            piece.setSquareID(squareID);
            player.setPiece(piece);

            if (player.getWallet() <= 0)
            {
                endGame = true;
                std::cout << "Gracz " << player.getName() << " przegral. Koniec gry!!!" << std::endl;
                break;
            }
        }
    }
}
