#include "startsquare.h"

qint32 StartSquare::atTheEntry(qint32 wallet)
{
    return wallet + 200;
}

qint32 StartSquare::atTheExit(qint32 wallet)
{
    return wallet;
}

qint32 StartSquare::passOver(qint32 wallet)
{
    return wallet + 200;
}

StartSquare::StartSquare()
{
    name = "Start Square";
}
