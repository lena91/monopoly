#ifndef PENALTYSQUARE_H
#define PENALTYSQUARE_H
#include "square.h"

class PenaltySquare : public Square
{
public:
    qint32 atTheEntry(qint32 wallet);
    qint32 atTheExit(qint32 wallet);
    qint32 passOver(qint32 wallet);
    PenaltySquare();
};

#endif // PENALTYSQUARE_H
