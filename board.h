#ifndef BOARD_H
#define BOARD_H
#include "square.h"
#include "startsquare.h"
#include "prizesquare.h"
#include "penaltysquare.h"
#include "depositsquare.h"
#include "blanksquare.h"
#include <QtGlobal>
#include <vector>
#include <algorithm>
#include <random>

class Board
{
public:
    Board(quint32 noOfSquares);
    std::vector<Square *> getSquaresVector() const;
    quint32 getNoOfSquares() const;

protected:
    quint32 noOfSquares;
    std::vector<Square*> squaresVector;
};

#endif // BOARD_H
