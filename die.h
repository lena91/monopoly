#ifndef DIE_H
#define DIE_H
#include <QtGlobal>
#include <ctime>

class Die
{
public:
    Die(quint8 noOfFaces);
    quint8 randomRoll() const;
protected:
    quint8 noOfFaces;
};

#endif // DIE_H
