#include "penaltysquare.h"

qint32 PenaltySquare::atTheEntry(qint32 wallet)
{
    return wallet - 1000;
}

qint32 PenaltySquare::atTheExit(qint32 wallet)
{
    return wallet;
}

qint32 PenaltySquare::passOver(qint32 wallet)
{
    return wallet;
}

PenaltySquare::PenaltySquare()
{
    name = "Penalty Square";
}
