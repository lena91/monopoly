#include "player.h"
#include <utility>

Player::Player(std::string name, Piece piece, qint32 wallet)
    : name(std::move(name)), piece(piece), wallet(wallet)
{

}

std::string Player::getName() const
{
    return name;
}

Piece Player::getPiece() const
{
    return piece;
}

void Player::setPiece(const Piece &value)
{
    piece = value;
}

qint32 Player::getWallet() const
{
    return wallet;
}

void Player::setWallet(const qint32 &value)
{
    wallet = value;
}
