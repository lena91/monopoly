#include "textformat.h"

TextFormat::TextFormat()
{

}

std::string TextFormat::colorText(quint8 colorID, std::string text)
{
    std::string coloredText =  "\033[1;" + std::to_string(colorID) + "m" + text + "\033[0m";
    return coloredText;
}
