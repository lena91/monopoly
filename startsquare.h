#ifndef STARTSQUARE_H
#define STARTSQUARE_H
#include "square.h"

class StartSquare : public Square
{
public:
    qint32 atTheEntry(qint32 wallet);
    qint32 atTheExit(qint32 wallet);
    qint32 passOver(qint32 wallet);
    StartSquare();
};

#endif // STARTSQUARE_H
