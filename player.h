#ifndef PLAYER_H
#define PLAYER_H
#include "piece.h"
#include <string>
#include <QtGlobal>

class Player
{
public:
    Player(std::string name, Piece piece, qint32 wallet);
    std::string getName() const;
    Piece getPiece() const;
    void setPiece(const Piece &value);
    qint32 getWallet() const;
    void setWallet(const qint32 &value);

private:
    std::string name;
    Piece piece;
    qint32 wallet;
};

#endif // PLAYER_H
