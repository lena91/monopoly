#ifndef DEPOSITSQUARE_H
#define DEPOSITSQUARE_H
#include "square.h"

class DepositSquare : public Square
{
public:
    qint32 atTheEntry(qint32 wallet);
    qint32 atTheExit(qint32 wallet);
    qint32 passOver(qint32 wallet);
    DepositSquare();
private:
    qint32 deposit = 0;
};

#endif // DEPOSITSQUARE_H
