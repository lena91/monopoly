#ifndef PIECE_H
#define PIECE_H
#include <QtGlobal>

class Piece
{
public:
    enum class Color
    {
        RED = 31,
        GREEN,
        YELLOW,
        BLUE,
        MAGENTA,
        CYAN,
        WHITE,
    };
    Piece(Color color);
    Color color;
    quint8 getSquareID() const;
    void setSquareID(const quint8 &value);
    Color getColor() const;

private:
    quint8 squareID = 0;
};

#endif // PIECE_H
