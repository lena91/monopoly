#ifndef PRIZESQUARE_H
#define PRIZESQUARE_H
#include "square.h"

class PrizeSquare : public Square
{
public:
    qint32 atTheEntry(qint32 wallet);
    qint32 atTheExit(qint32 wallet);
    qint32 passOver(qint32 wallet);
    PrizeSquare();
};

#endif // PRIZESQUARE_H
