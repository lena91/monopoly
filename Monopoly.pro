QT -= gui

CONFIG += c++17 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
    player.cpp \
    piece.cpp \
    board.cpp \
    square.cpp \
    prizesquare.cpp \
    penaltysquare.cpp \
    startsquare.cpp \
    blanksquare.cpp \
    monopolygame.cpp \
    die.cpp \
    depositsquare.cpp \
    monopolygamecreator.cpp \
    textformat.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    player.h \
    piece.h \
    board.h \
    square.h \
    prizesquare.h \
    penaltysquare.h \
    startsquare.h \
    blanksquare.h \
    monopolygame.h \
    die.h \
    depositsquare.h \
    monopolygamecreator.h \
    textformat.h
