#include "board.h"

Board::Board(quint32 noOfSquares)
{
    this->noOfSquares = noOfSquares;

    squaresVector.push_back(new PrizeSquare());
    squaresVector.push_back(new PenaltySquare());
    squaresVector.push_back(new DepositSquare());

    for (int i = 0; i < static_cast<int>(noOfSquares) - 4; ++i)
    {
        squaresVector.push_back(new BlankSquare());
    }

    std::random_device rd;
    std::default_random_engine dre{rd()};
    std::shuffle(squaresVector.begin(), squaresVector.end(), dre);

    squaresVector.insert(squaresVector.begin(), new StartSquare());
}

std::vector<Square *> Board::getSquaresVector() const
{
    return squaresVector;
}

quint32 Board::getNoOfSquares() const
{
    return noOfSquares;
}
