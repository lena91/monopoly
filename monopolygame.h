#ifndef MONOPOLYGAME_H
#define MONOPOLYGAME_H
#include <QtGlobal>
#include <QString>
#include <vector>
#include <iostream>
#include "die.h"
#include "player.h"
#include "piece.h"
#include "board.h"

class MonopolyGame
{
public:
    MonopolyGame(std::vector<std::string> playersNames, std::vector<quint8> diceSides, quint32 boardSize);
    void PlayGame();
protected:
    std::vector<Die> diceVector;
    std::vector<Player> playersVector;
    Board *board;
};

#endif // MONOPOLYGAME_H
